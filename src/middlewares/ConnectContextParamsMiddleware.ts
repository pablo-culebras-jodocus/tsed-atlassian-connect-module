import { Middleware, Req } from '@tsed/common';
import { ConnectApplication } from '../decorators/ConnectApplicationDecorator';

// TODO: Check if this declaration is enough to wrap the context within the request
// after Connect has been initialised
declare global {
    namespace Express {
        export interface Request {
            context: {
                title: string;
                addonKey: string;
                token: string;
                clientKey: string;
                userAccountId: string;
                localBaseUrl: string;
                hostBaseUrl: string;
                app: any;
            } 
        }
    }
}
  
@Middleware()
export class ConnectContextParamsMiddleware {
    constructor(@ConnectApplication private connectApp: any) {}

    use(@Req() req: Req) {
        req.app = this.connectApp;
        req.context = req.context;
    }
}