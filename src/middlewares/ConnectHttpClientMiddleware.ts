import { ConnectHttpClient } from '../services/ConnectHttpClient';
import { Middleware, Req } from '@tsed/common';
import { ConnectApplication } from '../decorators/ConnectApplicationDecorator';

declare global {
	namespace Express {
		export interface Request {
			atlassianHttpClient: ConnectHttpClient
		}
	}
}

@Middleware()
export class CreateConnectHttpClientMiddleware {
	constructor(@ConnectApplication private connectApp: any) {}
	
	use(@Req() req: Req) {
		req.atlassianHttpClient = req.atlassianHttpClient || new ConnectHttpClient(this.connectApp, req)
	}
}