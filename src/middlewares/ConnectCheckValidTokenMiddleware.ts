import { Middleware, Next, Req, Res, EndpointInfo } from "@tsed/common";
import { ConnectApplication } from '../decorators/ConnectApplicationDecorator';

/**
 * Provides the middleware which authenticates outgoing requests
 * back to the server.
 * 
 * @middleware
 */
@Middleware()
export class CreateConnectCheckValidTokenMiddleware {

    constructor(@ConnectApplication private connectApplication: any) {}
    
    use(@Req() req: Req, @Res() res: Res, @Next() next: Next, @EndpointInfo() endpoint: EndpointInfo) {
        // In a test environment, we do not need to authenticate the calls
        if(process.env.ENV === "test") {
            return next();
        }
        
        const options = endpoint.get(CreateConnectCheckValidTokenMiddleware) || {};
    
        return this.connectApplication.checkValidToken(options)(req, res, next)
    }
}