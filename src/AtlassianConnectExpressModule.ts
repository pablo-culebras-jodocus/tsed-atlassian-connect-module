import { ExpressApplication, Module, ProviderScope } from '@tsed/common';
import { ConnectApplication } from './decorators/ConnectApplicationDecorator';
import { ConnectService } from './services/ConnectService';

@Module({
    scope: ProviderScope.SINGLETON,
    deps: [ConnectService]
})
export class AtlassianConnectExpressModule {
    
    constructor(@ExpressApplication private expressApplication: ExpressApplication,
        @ConnectApplication private connectApplication: any) {}
}