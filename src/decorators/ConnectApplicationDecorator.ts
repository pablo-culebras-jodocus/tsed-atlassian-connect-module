import { Type } from "@tsed/core";
import { Inject } from "@tsed/di";
import { ATLASSIAN_CONNECT_APP } from '../services/AtlassianConnectExpressAppProvider';

export type HttpClientParams = {
    clientKey: string
}

/**
 * Inject the express application instance.
 *  
 * ```typescript
 * import {ConnectApplication, Service, Inject} from "@tsed/common";
 * 
 * @Service()
 * export default class OtherService {
 *   constructor(@ConnectApplication expressApplication: any) {}
 * }
 * ```
 * 
 * @param {Type<any>} target
 * @param {string} targetKey
 * @param {TypedPropertyDescriptor<Function> | number} descriptor
 * @returns {any}
 * @decorator
 **/
export function ConnectApplication(target: Type<any>, targetKey: string,
    descriptor: TypedPropertyDescriptor<Function> | number): any {
        return Inject(ATLASSIAN_CONNECT_APP)(target, targetKey, descriptor);
    }