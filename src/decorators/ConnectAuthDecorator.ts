import { UseAuth } from "@tsed/common";
import { applyDecorators } from "@tsed/core";
import { CreateConnectAuthenticateMiddleware } from "../middlewares/ConnectAuthenticationMiddleware";
import { CreateConnectCheckValidTokenMiddleware } from '../middlewares/ConnectCheckValidTokenMiddleware';

export function ConnectAuthenticate(options: any = {}) {
    return applyDecorators(
        UseAuth(CreateConnectAuthenticateMiddleware, options)
    );
}
    
export function ConnectCheckValidToken(options: any = {}) {
    return applyDecorators(
        UseAuth(CreateConnectCheckValidTokenMiddleware, options)
    )
}