import { Req, Use } from "@tsed/common";
import { decoratorArgs, Type } from "@tsed/core";
import { CreateConnectHttpClientMiddleware } from "../middlewares/ConnectHttpClientMiddleware";

export function ConnectHttpClientParam() {
  return (target: Type<any>, property: string, index: number): void => {
      // Add CreateConnectHttpClientMiddleware. This middleware will be called before the endpoint
      Use(CreateConnectHttpClientMiddleware)(...decoratorArgs(target, property))
      // And use the param injector. It'll pick the httpClient from req.atlassianHttpClient add my the CreateHttpClientMiddleware
      Req("atlassianHttpClient")(target, property, index)
  } 
}