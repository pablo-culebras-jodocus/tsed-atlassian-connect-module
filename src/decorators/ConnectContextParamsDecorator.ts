import { Req, Use } from "@tsed/common";
import { decoratorArgs, Type } from "@tsed/core";
import { ConnectContextParamsMiddleware } from "../middlewares/ConnectContextParamsMiddleware";

export function ConnectContextParam() {
  return (target: Type<any>, property: string, index: number): void => {
      // Add ConnectContextParamsMiddleware. This middleware will be called before the endpoint
      Use(ConnectContextParamsMiddleware)(...decoratorArgs(target, property))
      // And use the param injector. It'll pick the context from req.context add my the ConnectContextParamsMiddleware
      Req("context")(target, property, index)
  } 
}