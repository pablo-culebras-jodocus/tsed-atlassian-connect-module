import { $log, Service } from "@tsed/common";
import { ConnectApplication } from "../decorators/ConnectApplicationDecorator";

@Service()
export class ConnectService {

    constructor(@ConnectApplication private connectApp: any) {
        $log.info("Initializing ConnectService");
    }
    
    /**
     * Retrieves the Key of the Connect App.
     * 
     * @returns {string}
     */
    public getAppKey(): string {
        return this.connectApp.key;
    }

    public async load<T>(key: string, clientKey: string = "global"): Promise<T> {
        return this.connectApp.settings.get(key, clientKey);
    }

    public async store<T>(key: string, object: any, clientKey: string): Promise<T> {
        return this.connectApp.settings.set(key, object, clientKey)
    }
}