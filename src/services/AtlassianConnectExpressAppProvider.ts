import { Configuration, ExpressApplication, registerProvider } from "@tsed/common";
import * as ace from "atlassian-connect-express";

export const ATLASSIAN_CONNECT_APP = Symbol.for("ATLASSIAN_CONNECT_APP");

registerProvider({
	provide: ATLASSIAN_CONNECT_APP,
	deps: [ExpressApplication, Configuration],
	useFactory(expressApp: ExpressApplication, configuration: Configuration) {
		const connectApp = ace(expressApp, configuration.get("atlassianConnect") || {});
        expressApp.use(connectApp.middleware());
        
		if (process.env.ENV === "development") {
			connectApp.register();
		}

        return connectApp;
	}
});