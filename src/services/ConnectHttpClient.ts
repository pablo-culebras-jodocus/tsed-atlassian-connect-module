export interface AtlassianRequestOptions {
    headers: {
        [key: string]: string;
    },
    [key: string]: any
}

export class ConnectHttpClient {
    private httpClient: any;
    
    constructor(connectApp: any, request: Express.Request) {
        this.httpClient = connectApp.httpClient(request); 
    }
    
    public get(url: string, options: Partial<AtlassianRequestOptions> = {}): Promise<any> {     
        return this.execute("get", url, options);
    }

    public post(url: string, options: Partial<AtlassianRequestOptions> = {}): Promise<any> {     
        return this.execute("post", url, options);
    }

    public put(url: string, options: Partial<AtlassianRequestOptions> = {}): Promise<any> {     
        return this.execute("put", url, options);
    }

    public delete(url: string, options: Partial<AtlassianRequestOptions> = {}): Promise<any> {     
        return this.execute("del", url, options);
    }
    
    private execute(method: string, url: string,
        options: Partial<AtlassianRequestOptions>): Promise<any> {
        return new Promise((resolve, reject) => {
            this.httpClient[method]({
                url,
                ...options,
            }, (error: any, response: any, body: any) => {
                error ? reject(error) : resolve({response, body})
            })
        });
    }
}