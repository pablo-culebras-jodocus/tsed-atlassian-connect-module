To consume the pattern library in your app, we recommend installing via npm. If you want to install a specific version range, you can:

```
npm install --save git@bitbucket.org:pablo-culebras-jodocus/tsed-atlassian-connect-module.git#semver:^v1.0.7
```